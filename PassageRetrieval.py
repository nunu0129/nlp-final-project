import argparse
import operator
import math
import Parser
"""parser = argparse.ArgumentParser()
parser.add_argument("test")
parser.add_argument("question")
parser.add_argument("threshold", type=int)
args = parser.parse_args()
testF = args.test
question = args.question
threshold = args.threshold"""

def stringMatch(question, n, testF):
  """
  requires:
   1) string containing a question 2) a positive int
   3) string containing testFile they want to use
  returns:
   a list of tuples (p,s) of size of at most n where p in range [0,100],
  """
  sentences = Parser.grabSentences(testF)
  q = question.rstrip(' ?.\n').lstrip(' ?.\n').lower()
  lq = q.split()
  ansnP = []
  for i in range(len(sentences)):
    c = 0
    for w in lq:
      if w in sentences[i].lower():
        c+=1
    p = float("%.2f" % (c*100.0/len(lq)))
    if(p>0):
      ansnP.append((p,sentences[i]))
  ansnP.sort()
  ansnP.reverse()
  if len(ansnP) < math.fabs(n):
    return ansnP
  else:
    return ansnP[0:n]
