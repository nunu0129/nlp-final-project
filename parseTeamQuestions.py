import sys, re

qs = open("questionFiles.txt")
tempfile = open("fileAndQuestion", 'w')

for line in qs:
    allData = line.split()
    fileLoc = allData[-1]
    question = (" ".join(line.split()[:-1]))[1:-1]
    tempfile.write(fileLoc + " " + question + "\n")
