import sys, nltk

#finds the first noun in the pos tagged sentence

def findNoun(pos):
    for i in range(len(pos)):
        (w, p) = pos[i]
        if(p[0] == "N"):
            return w
        if(p[0] == "V"):
            return "unknown"
    return "unknown"

# Takes a question as an agrument
# returns (pos, string) where pos is a list of probable
# POS tags and string is a more specific classification

def classify(question):
    truefalsekeys = ["is", "was", "has", "are", "were", "have", "can",
                     "will", "does", "do", "did", "should", "could",
                     "would", "were"]

    tokens = nltk.word_tokenize(question)
    pos = nltk.pos_tag(tokens)

    (word1, pos1) = pos[0]
    word1 = str.lower(word1)
    (word2, pos2) = pos[1]
    word2 = str.lower(word2)

    if(word1 == "who"):
        if(pos2 == "VBZ"): #singular verb
            return (["NNP"], "person")
        return (["NNP", "NNPS"], "person") #the non-3rd person singular isn't reliable
    if(word1 == "when"):
        return (["CD", "NP"], "date") #year/time = number, month/dayofweek = NP
    if(word1 in truefalsekeys):
        return ([], "yes/no")
    if(word1 == "where"):
        return (["NNP", "NN"], "place")
    if(word1 == "how" and (pos2  in ["JJ", "RB"])):
        return (["CD"], "number")
    if((pos1 == "IN" and word2 in ["what", "which"] )
         or word1 in ["what", "which"]):
        noun = findNoun(pos)
        if (noun != "unknown"):
            return (["NNP", "NN", "NNPS", "NNS", "CD"], noun)
    if(pos1 == "IN"):
        return classify(question[question.index(",") + 2 : ])
    if(word1 in ["what"]):
        return ([], "what")
    if(tokens[0] in ["why", "how"]):
        return ([], "phrase")
    return ([], "unknown")



if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: python questionClassification.py question"
    else:
        print classify(sys.argv[1])
