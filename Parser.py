import operator
import math
import nltk
import pickle
import codecs, unidecode

def grabSentences(test):
  """requires:
  1) path to article (string)"""
  f = codecs.open(test,encoding='utf-8')
  tooMany = unidecode.unidecode(f.read()).split('\n')
  f.close()
  sentences = []
  for l in tooMany:
    for er in nltk.sent_tokenize(l):
      if (er == "See also") or (er=="Citations") or (er=="References"):
        return sentences
      if (len(er) >0):
        sentences.append(er.rstrip(' \t').lstrip(' \t'))
  return sentences

def isPerson(file):
  f = open(file)
  article = f.read()
  f.close()
  f = open(file)
  sent1 = f.readline().rstrip('\n')
  f.close()
  pM = article.count("He")
  pF = article.count("She")
  o = article.count("It")
  #print(str(o)+","+str(pM)+","+str(pF))
  if "film" in sent1 or "constellation" in sent1 or "anguage" in sent1 or (o>pM and o>pF):
    return(0, sent1)
  return (1,sent1)

def getQ(corpus, diffW, picky, train):
  """requires:
  1) string to path to corpus 
  2) string "e","m" or "h" to identify difficulty interested in extracting
  3) picky. 0 if not picky about which article the question is not else non zero
  4) string of path to article we want questions for
  returns:
  dictionary that maps a question (string) to an array of answers (string)""" 
  f = open(corpus)
  lines = f.read().lower().split('\n')
  f.close()
  lines.pop(0)
  if (len(lines[len(lines)-1]) == 0):
    lines.pop()
  diff = "" 
  if ("e" in diffW.lower()):
    diff += "easy"
  if ("m" in diffW.lower()):
    diff += " medium"
  if ("h" in diffW.lower()):
    diff += " hard"
  print(diff)
  dic = {} 
  for l in lines:
    opts = l.split('\t')
    if((not picky or opts[3] == train) and opts[4] in diff):
      k = opts[5]
      if (k not in dic.keys()):
        dic[k] = [opts[8]]
      else:
        dic[k].append(opts[8])
  return dic

def makeFreqDict(test):
  t = open(test)
  text = t.read()
  t.close()
  words = nltk.word_tokenize(text)
  leDict = {}
  for w in words:
    if w in leDict.keys():
      leDict[w]+=1
    else:
      leDict[w]=1
  return leDict

def storeFreq(test):  
  d = makeFreqDict(test)
  f = (test.rstrip("tx"))+"p"
  pickle.dump(d,open(f,"wb"))
 
def getFreq(test):
  f = (test.rstrip(".tx"))+".p"
  return pickle.load(open(f, "rb"))
