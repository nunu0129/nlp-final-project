import questionClassification, sys, nltk, re, Parser
from nltk.tag import stanford

def stanfordTag(tokens):
     st = stanford.POSTagger('/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/stanford-postagger-2012-11-11/models/english-left3words-distsim.tagger','/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/stanford-postagger-2012-11-11/stanford-postagger.jar')
     return st.batch_tag(tokens)

#tags either using stanford or nltk
def pos_tag(tokens):
    return stanfordTag(tokens)
    #return nltk.pos_tag(tokens)

def countNegatives(sentence):
    negatives = [" no ", "No ", " not ", "Not ", " never " ,
                 "Never ", "n't"]
    count = 0
    for neg in negatives:
        if neg in sentence:
            count += 1

    return count

def getNameDict(fileName):
    nameFile = open(fileName)
    allNames = {}
    for line in nameFile:
        [name, freq, cumulative, rank] = line.split()
        name = name.lower()
        if(name in allNames):
            allNames[name] = min(allNames[name], rank)
        else:
            allNames[name] = rank

    nameFile.close()
    return allNames

def getCityDict(fileName):
    cityFile = open(fileName)
    cities = {}
    for line in cityFile:
        [city, freq] = line.split()
        cities[city.lower()] = freq
    cityFile.close()
    return cities

def getData(fileName):
    unitFile = open(fileName)
    units = set([])
    for line in unitFile:
        units.add(line[:-1]) #get rid of \n
    unitFile.close()
    return units

def months():
    return set(["January", "February", "March",
                "April", "May", "June", "July",
                "August", "September",
                "October", "November", "December"])
def seasons():
    return set(["winter", "Winter", "spring",
                "Spring", "Summer", "summer",
                "Fall", "fall", "Autumn", "autumn"])

def days():
    return set(["Sunday", "Monday", "Tuesday",
                "Wednesday", "Thursday",
                "Friday", "Saturday"])

#finds all the expected parts of speech in passage that aren't in question
def findPOS(posPass, question, pos):
    nnpSet = set([])
    currNNP = ""
    for i in range(len(posPass)):
        (w, p) = posPass[i]
        if (p in pos and w not in question) :
            if(currNNP == ""):
                currNNP = w
            else:
                currNNP += " " + w
        elif(currNNP != ""):
            nnpSet.add(currNNP)
            currNNP = ""
    return nnpSet

#given a file of common names and a set of possible candidate,
#find the most likely answer (assumes file has single words per line)
def mostLikelySingleAnswer(parseFile, fileName, nnpSet, compare, default):
    if(len(nnpSet) > 1):
        bestName = "I don't know."
        rank = float(default)
        allNames = parseFile(fileName)
        for fullName in nnpSet:
            currRank = float("inf")
            for name in fullName.split():
                if(name.lower() in allNames):
                    if(currRank == float("inf")):
                        currRank = float(allNames[name.lower()])
                    else:
                        currRank = compare(currRank, float(allNames[name.lower()]))
            if(compare (currRank, rank) == currRank):
                 rank = currRank
                 bestName = fullName
        return bestName
    return "I don't know."

#returns most likely answer from a file
#whose lines are each a full name (and there aren't any ranks)
def mostLikelyMultipleAnswer(fileName, nnpSet):
     data = getData(fileName)
     for name in nnpSet:
          if(name in data):
               return name
     return "I don't know."

#returns an answer and a confidence level for that answer
def findSingleAnswer (question, passage, posPass, tokens, prob, article, isPerson, subject):
     threshold = 65
     confidence = prob
     (pos, tag) = questionClassification.classify(question)
     previous = False

     if(tag == "yes/no"):
          if(prob > threshold):
               if(countNegatives(question) % 2 == countNegatives(passage) % 2):
                    return ("Yes.", confidence)
          return ("No.", confidence)

     elif (tag == "person"):
          #if subject is a pronoun, look to previous sentence for reference
          if(tokens[0] in ["He", "he", "She", "she"]):
               sents = Parser.grabSentences(article)
               if passage in sents and sents.index(passage) != 0:
                    pindex = sents.index(passage) - 1
                    newP = sents[pindex]
                    tokens2 = nltk.word_tokenize(newP)
                    tokenized2 = [tokens2]
                    posPasses2 = pos_tag(tokenized2)
                    posPass2 = posPasses2[0]
                    (name, conf) = findSingleAnswer(question, newP, posPass2,
                                                    tokens2, prob, article,
                                                    isPerson, subject)
                    if(len(name.split()) <= 3):
                         return (name, conf)
                    else:
                         passage = name
                         posPass = posPass2
                         previous = True

          nnpSet = findPOS(posPass, question, pos)
          wordsToRemove = []
          for word in nnpSet:
               firstWord = word.split()[0]
               if((firstWord in tokens) and tokens.index(firstWord) > 0):
                    ind = tokens.index(firstWord) - 1
                    (w, p) = posPass[ind]
                    if(p in ["IN", "TO"]):
                         wordsToRemove.append(word)
          for word in wordsToRemove:
               nnpSet.remove(word)

          #if there's a proper noun  right before the second word
          #in the question (or a pronoun),
          #it's likely to be the answer
          questionAnchor = question.split()[1]
          if(len(questionAnchor) > 3 and questionAnchor in tokens):
               ind = tokens.index(questionAnchor)
               for word in nnpSet:
                    lastWord = word.split()[-1]
                    if(lastWord in tokens and
                       tokens.index(lastWord) + 1 == ind):
                         return (word, confidence + 30)
                    elif(ind >= 0 and tokens[ind-1] in ["he", "He", "she", "She"]):
                         sents = Parser.grabSentences(article)
                         if passage in sents and sents.index(passage) != 0:
                              pindex = sents.index(passage) - 1
                              newP = sents[pindex]
                              tokens2 = nltk.word_tokenize(newP)
                              tokenized2 = [tokens2]
                              posPasses2 = pos_tag(tokenized2)
                              posPass2 = posPasses2[0]
                              (name, conf) = findSingleAnswer(question, newP, posPass2,
                                                              tokens2, prob, article,
                                                              isPerson, subject)
                              if(len(name.split()) <= 3):
                                    return (name, conf)
                              else:
                                   passage = name
                                   posPass = posPass2
                                   previous = True

          #else, choose the name most likely to be a person
          person = mostLikelySingleAnswer(getNameDict, "/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/censusData/dist.all", nnpSet, min, 20000)
          if person != "I don't know." :
               return (person, confidence + 10)
          else:
               confidence -= 10
               #default to the subject of the article if it's about a person
               if(isPerson == 1):
                    return (subject, confidence)
               # if we've returned the previous passage, just return some proper noun
               nnpSet = findPOS(posPass, question, pos)
               if(previous and len(nnpSet) > 0):
                    person = mostLikelySingleAnswer(getNameDict, "/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/censusData/dist.all", nnpSet, min, float("inf"))
                    if person != "I don't know." :
                         return (person, confidence)
                    else:
                         return (nnpSet.pop(), confidence - 5)

     elif(tag == "number"):
          units = getData("/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/censusData/units.txt")
          best = "I don't know."
          for (w, p) in posPass:
               if (p == "CD" and w not in question):
                    i = tokens.index(w)
                    if(i > 0 and tokens[i-1] == "$"):
                         w = "$" + w
                         best = w
                         confidence += 10
                    while(i + 1 < len(tokens) and tokens[i + 1] in units):
                         w += " " + tokens[i+1]
                         i += 1
                         best = w
                         confidence += 10
                    if(best == "I don't know."):
                         best = w
                    else:
                         return (best, confidence)
          if(best != "I don't know."):
               return (best, confidence)
          else:
               confidence -= 20

     elif(tag == "time"):
          units = getData("/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/censusData/units.txt")
          for (w, p) in posPass:
               if (p == "CD" and w not in question):
                    return (w, confidence + 10)
          confidence -= 20

     elif(tag == "date"):
          monthNames = months()
          seasonNames = seasons()
          daysOfWeek = days()
          best = ""
          for i in range(len(posPass)):
               (w, p) = posPass[i]
               if(w not in question):
                    if w in seasonNames or w in daysOfWeek:
                         return (w, confidence + 10)
                    elif w in monthNames:
                         if(i + 1 < len(posPass)):
                              (w2, p2) = posPass[i+1]
                              if(p2 == "CD"): #i.e. January 1st
                                   w += " " + w2
                                   if(i + 2 < len(posPass)):
                                        (w3, p3) = posPass[i+2]
                                        if(p3 == "," and i + 3 < len(posPass)):
                                             (w4, p4) = posPass[i+3]
                                             if(p4 == "CD"):  #i.e. January 1, 2013
                                                  w += ", " + w4
                                        elif(p3 == "CD"): #i.e. January 1 2013
                                             w += " " + w3
                         return (w, confidence + 20)
                    elif p == "CD":
                         if(i + 1 < len(posPass)):
                              (w2, p2) = posPass[i+1]
                              if(w2 in ["BC", "BCE", "AD", "CE"]):
                                   return (w + " " + w2, confidence + 20)
                         if(len(best) != 4):
                              best = w
          if best != "":
               if(len(best) == 4):
                    confidence += 10
               return (best, confidence)
          else:
               confidence -= 10

     elif(tag == "place"):
          nnpSet = findPOS(posPass, question, ["NNP", "NNPS"])
          country = mostLikelyMultipleAnswer("/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/censusData/countries.txt", nnpSet)
          if(country != "I don't know."):
               return (country, confidence + 15)
          city = mostLikelySingleAnswer(getCityDict, "/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/censusData/cities.txt", nnpSet, max, 0)
          if(city != "I don't know."):
               return (city, confidence + 10)
          nnpSet = findPOS(posPass, question, ["NN"])
          if(len(nnpSet) > 0):
               place = nnpSet.pop()
               placeList = place.split()
               if(len(placeList) > 1):
                    i = tokens.index(placeList[0])
               else:
                    i = tokens.index(place)
               if(i != 0):
                    (w, p) = posPass[i-1]
                    if(p == "DT"):
                         return (w + " " + place, confidence)
          confidence -= 10

     elif(tag == "city"):
          nnpSet = findPOS(posPass, question, ["NNP", "NNPS"])
          city = mostLikelySingleAnswer(getCityDict, "/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/censusData/cities.txt", nnpSet, max, 0)
          if city != "I don't know.":
               return (city, confidence + 10)
          else:
               confidence -= 10

     elif(tag == "country"):
          nnpSet = findPOS(posPass, question, ["NNP", "NNPS"])
          country = mostLikelyMultipleAnswer("/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/censusData/countries.txt", nnpSet)
          if country != "I don't know." :
               return (country, confidence + 20)
          else:
               confidence -= 20

     elif(tag == "state"):
          nnpSet = findPOS(posPass, question, ["NNP"])
          state = mostLikelyMultipleAnswer("/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/censusData/states.txt", nnpSet)
          if state != "I don't know." :
               return (state, confidence + 20)
          else:
               confidence -= 20

     elif(tag == "what"):
          if(tokens[0] in ["It", "it"]):
               sents = Parser.grabSentences(article)
               if passage in sents and sents.index(passage) != 0:
                    pindex = sents.index(passage) - 1
                    newP = sents[pindex]
                    tokens2 = nltk.word_tokenize(newP)
                    tokenized2 = [tokens2]
                    posPasses2 = pos_tag(tokenized2)
                    posPass2 = posPasses2[0]
                    return findSingleAnswer(question, newP, posPass2,
                                                    tokens2, prob, article,
                                                    isPerson, subject)

     return (passage, confidence)

#finds all the answers to all the questions
#if more than one passage is given, finds the one with the highest
#   confidence level
def findAnswer(questions, passages, article):
     text = []
     for passes in passages:
          for (prob, p) in passes:
               text.append(p)
     tokenized = map(lambda p : nltk.word_tokenize(p),text)
     posPasses = pos_tag(tokenized)
     answers = []
     count = 0
     (isPerson, subject) = Parser.isPerson(article)
     for ind in range(len(passages)):
          passagePossibilities = passages[ind]
          question = questions[ind]
          confidence = float("-inf")
          newAnswer = ""
          for (prob, passage) in passagePossibilities:
               posPass = posPasses[count]
               tokens = tokenized[count]
               (currAnswer, conf) = findSingleAnswer(question, passage, posPass, tokens, prob, article, isPerson, subject)
               currConfidence = float(conf)
               if(currConfidence > confidence):
                    newAnswer = currAnswer
                    confidence = currConfidence
               count += 1
          answers.append(newAnswer)
     return answers

#if __name__ == "__main__":
#    if len(sys.argv) < 4:
#        print "Usage: python answerRetrieval.py question passage prob"
#    else:
#        print findAnswer([sys.argv[1]], [sys.argv[2]], [float(sys.argv[3])])
