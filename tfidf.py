import nltk
import Parser
import math
import pickle

def ListofDicts():
  #assuming there are only 40 documents
  D = []
  test = "/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/set"
  for i in range(4):
    begin= str(i+1)
    for j in range(10):
      end = "/a"+str(j+1)+".p"
      d = pickle.load(open(test+begin+end,"rb"))             
      D.append(d)
  return D

def idf(word,D):
  total = len(D)+0.0
  freq = 0
  for d in D:
    if (word in d.keys()):
      freq+=1
  if freq == 0:
    freq = 1.0
    total+=1
  #print(word+" idf: "+str(math.log(total/freq)))
  return math.log(total/freq)

def tf(word,test):
  f = open(test)
  article = f.read()#.lower()
  f.close()
  count = article.count(word)#.lower())
  #print(word+" tf: "+str(count))
  if count:
    return 1
  return 0

def tfidf(question, n, testF):
  """
  requires:
   1) string containing a question 2) a positive int
   3) string containing testFile they want to use
  returns:
   a list of tuples (p,s) of size of at most n where p in range [0,100],
  """
  sentences = Parser.grabSentences(testF)
  Q = question.rstrip(' ?.\n').lstrip(' ?.\n')
  ll = nltk.word_tokenize(Q) 
  lq = list(ll)
  comm1 = ["Who","What","When","Why","How","Where","Did","Was","Were","Is","Have","Has","Can","Do","Does"]
  comm2 = ["How much", "How many", "When did","When was", "How long", "Who are", "Who is","How far","When did", "Why does"]
  if (ll[0]+" "+ll[1]) in comm2:
    lq = list(ll[2:len(ll)])
  elif ll[0] in comm1:
    lq = list(ll[1:len(ll)])
  ansnP = []
  LD = ListofDicts()
  V = []
  
  tot = 0
  if len(lq) == 0:
    return ansnP
  for w in lq:
    v = idf(w,LD)*tf(w,testF)
    tot+=v
    #print(w+": "+str(v))
    V.append((w,v))

  for s in sentences:
    c = 0.0
    sl = nltk.word_tokenize(s.lower())
    for i in range(len(V)):
      w = V[i][0]
      val = V[i][1] 
      if ((len(w)>1) and w.lower() in s.lower()) or (w.lower() in sl):
        c+=val
    p = float("%.2f" % (c*100.0/tot))
    if(p>0):
      ansnP.append((p,s))

  ansnP.sort()
  ansnP.reverse()
  if len(ansnP) < n:
    return ansnP
  else:
    return ansnP[0:n]

#print(tfidf("Is Lionel Messi a father?", 3, "set1/a7.txt"))
