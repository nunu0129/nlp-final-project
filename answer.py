import sys, PassageRetrieval, answerRetrieval, tfidf

if (len(sys.argv) < 3):
    print "Usage: python answer.py article questionsFile"
else:
    article = sys.argv[1]
    questionFile = open(sys.argv[2])
    questions = []
    passages = []
    for question in questionFile:
        if(not question.strip()):
            continue
        questions.append(question[:-1])
        currPassages = tfidf.tfidf(question, 3, article)
        passages.append(currPassages)
    answers = answerRetrieval.findAnswer(questions, passages, article)
    for answer in answers:
        print answer
