import sys, os, subprocess, tempfile, nltk

def main(filename):
  parsed, trees = parseDoc(filename)
  for s in parsed:
    print s

def parseSentences(sentences):
  f, fpath = tempfile.mkstemp(text=True)
  f = os.fdopen(f,'w')
  for s in sentences[:200]:
    if len(s)>30:
      f.write(s+' ')
  f.close()
  parse = parseDoc(fpath)
  os.unlink(fpath)
  return parse

def parseDoc(fpath):
  base = '/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/'
  p = subprocess.Popen([base+'stanford-parser-2012-11-12/lexparser.sh',
      fpath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  errcode = p.wait()
  out = p.stdout.read().split('\n')
  sentences = []
  trees = []
  for line in out:
    if not line.strip(): continue
    if line[0]=='(':
      sentences.append(line.strip())
      trees.append(nltk.tree.Tree.parse(line.strip()))
  return sentences, trees

if __name__=='__main__':
  if len(sys.argv)!=2:
    print 'Usage: python stanford.py file'
  else:
    main(sys.argv[1])
