import subprocess, tempfile, sys, os

def main(filename):
  print simplifyFile(filename)

#takes filename and returns list of simplified sentences
def simplifyFile(filename):
  return callQG(filename)

#takes list of sentences and returns list of simplified sentences
def simplifySentences(sentences):
  (f,fpath) = tempfile.mkstemp(text=True)
  f = os.fdopen(f,'w')
  for s in sentences:
    f.write(s+' ')
  f.close()
  simple = callQG(fpath)
  os.unlink(fpath)
  return simple

def callQG(file):
  base = '/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/'
  p = subprocess.Popen([base+'FactualStatementExtractor/simplify.sh',file],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  errcode = p.wait()
  out = p.stdout.read().split('\n')
  return [s for s in out if s.strip()]

if __name__ == '__main__':
  if len(sys.argv)!=2:
    print 'Usage: python simplify.py file'
  else: main(sys.argv[1])
