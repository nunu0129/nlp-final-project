import nltk, sys, re
from nltk.corpus imort wordnet as wn

#Get question keywords for passage retrieval
#Return: list of strings
def questionKeywords(question):
  verbstop = Set(['is','was','has','does','did','do','have','are'])
  tagged = nltk.pos_tag(nltk.word_tokenize(question))
  list = []
  #grab any noun phrases (add stoplist to get rid of useless words?)
  #give higher weight to NNPs? How do we want to do weighting?
  #should get the verb if it isn't common (another stoplist?)
  for pair in tagged:
    if pair[1][0]=='N': 
      list.append(pair[0])
    elif pair[1][0]=='V' and pair[0] not in verbstop:
      list.append(pair[0])
  return list

#get synonyms: may be unhelpful when meaning ambiguous
#Return: set of synonyms, original word included
def getSynonyms(word):
  syns = wn.synsets(word)
  synset = Set([word.lower()])
  for s in syns:
    for w in s.lemma_names:
      synset.add(w)
  return synset

if __name__=='__main__':
  if len(sys.argv) < 2:
    print 'Usage: python questionFormulation.py question'
  else:
    questionKeywords(sys.argv[1])
