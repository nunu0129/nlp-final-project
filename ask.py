import sys, os, nltk, re, subprocess, tempfile, time
import Parser, simplify, answerRetrieval, stanford
from nltk.tree import *

def main(filename, nquestions):
  base = '/afs/andrew.cmu.edu/usr8/lscharff/11411/nlp-final-project/'
  sentences = [s for s in Parser.grabSentences(filename) if
               len(s)>30 and s.find(':')==-1 and s.find(r'*')==-1]
  t0 = time.time()
  simple = simplify.simplifySentences(sentences[:200])
  t1 = time.time()
  #print 'simplify',str(len(simple)), str(t1-t0)
  parsed, trees = stanford.parseSentences(simple)
  t2 = time.time()
  #print 'parsed', str(t2-t1)
  #parsed, trees = stanford.parseSentences(sentences)
  verbs = getVerbConjDict(base+'FactualStatementExtractor/verbConjugations.txt')
  allNames = answerRetrieval.getNameDict(base+'censusData/dist.all')
  whenWords = answerRetrieval.months().union(answerRetrieval.seasons()).union(
              answerRetrieval.days())
  places = getPlaces(base)
  qs = set()
  qs = getWhQuestions(parsed,whenWords,'When ',verbs,{'in','on'},('May','June'))
  if len(qs)<nquestions:
    qs = qs.union(getWhQuestions(parsed,places,'Where ',verbs,{'in'},
                 ('Russia','Bulgaria')))
  if len(qs)<nquestions:
    qs = qs.union(getWhoQuestions(parsed,trees,allNames,nquestions-len(qs)))
  if len(qs)<nquestions:
    yn = sorted(getMoreYNQuestions(parsed,verbs),key=lambda s: len(s))
    index = len(qs)-nquestions
    qs = qs.union(yn[index:])
    
  #if len(qs)<nquestions:
  #  qs = qs.union(getYNQuestions(sentences,nquestions-len(qs)))
  #print qs
  if len(qs)<nquestions:
    parsed, trees = stanford.parseSentences(sentences)
    qs = qs.union(getMoreYNQuestions(parsed,verbs))

  for i in range(1,nquestions+1):
    if not qs: break
    print qs.pop()

def getVerbConjDict(filename):
  verbFile = open(filename)
  verbs = {}
  for line in verbFile:
    line = line.strip().split()
    if len(line)<3: break
    verbs[line[2]] = line[0]
  return verbs

def getPlaces(base):
  places = set()
  f = open(base+'censusData/states.txt')
  for line in f:
    places.add(line.strip())
  f.close()
  f = open(base+'censusData/countries.txt')
  for line in f:
    places.add(line.strip())
  f.close()
  #f = open('censusData/cities.txt')
  #for line in f:
  #  city = line.strip().split()[0]
  #  places.add(city)
  #f.close()
  return places

def getWhQuestions(parsed, whSet, prefix, verbs, prepSet, replace):
  qs = set()
  auxVerbs = set(['was','is','are','were','will'])
  ignore = set(['has','had','have'])
  for i in range(0,len(parsed)):
    if re.search('PP',parsed[i]): #sentence contains a preposition phrase
      tree = nltk.tree.ParentedTree.parse(parsed[i])
      if tree[0].node!='S': continue
      if len(tree.leaves())<10: continue
      for s in tree.subtrees(lambda t: t.node=='PP'):
        if s.leaves()[0] not in prepSet: continue
        if sum(1 for i in s.subtrees(lambda t: t.node=='PP'))>1: continue
        if 'to' in s.leaves()[1:] or 'and' in s.leaves()[1:]: continue
        found = False
        for w in s.pos():
          if w[1]=='NNP' and w[0] in whSet: #need to add check for child PP
            #print parsed[i]
            subtree = s
            position = subtree.treeposition()
            pattern = re.compile('%(w_word)s'%{'w_word':w[0]})
            if w[0]==replace[0]:
              pattern2 = re.compile('\s%(w)s([a-z]+n{1})'%{'w':replace[1]})
            else:
              pattern2 = re.compile('\s%(w)s([a-z]+)'%{'w':replace[0]})
            tree[s.treeposition()] = '' #remove PP from tree
            found = True
            for vp in tree.subtrees(lambda t: t.node=='VP'):
              if len(vp.treeposition())!=2: continue
              if vp[0].node[:2]=='VB' and vp[0][0] in ignore: break
              #move modals and to be verbs to front of sentence
              if vp[0].node=='MD' or (len(vp[0])==1 
                  and vp[0][0] in auxVerbs):
                verb = vp[0][0]
                #remove verb phrase from tree
                tree[vp.treeposition()][0][0] = ''
                sent = [t for t in tree.leaves() if t.strip()]
                tree[position] = subtree
                sent2 = [t for t in tree.leaves() if t.strip()]
                #make first word lowercase if not a proper noun
                if tree.pos()[0][1]!='NNP':
                  sent[0] = sent[0].lower()
                  sent2[0] = sent2[0].lower()
                sent = ' '.join(sent[:-1])
                sent = sent.replace('-LRB-','(').replace('-RRB-',')')
                qs.add(prefix+verb+' '+sent+'?')
                if w[0]==replace[0]:
                  sent2 = re.sub(pattern,replace[1],' '.join(sent2[:-1]))
                  sent2 = re.sub(pattern2,' '+replace[1]+
                          'n',sent2)
                else:
                  sent2 = re.sub(pattern,replace[0],' '.join(sent2[:-1]))
                  sent2 = re.sub(pattern2,' '+replace[0]+
                          'n',sent2)
                sent2 = sent2.replace('-LRB-','(').replace('-RRB-',')')
                qs.add(verb.capitalize()+' '+sent2+'?')
                break
              #get base form of verb and replace
              elif vp[0].node[:2]=='VB' and vp[0][0] in verbs:
                base = verbs[vp[0][0]]
                #set verb to base form
                tree[vp.treeposition()][0][0] = base
                sent = [t for t in tree.leaves() if t.strip()]
                tree[position] = subtree
                sent2 = [t for t in tree.leaves() if t.strip()]
                if tree.pos()[0][1]!='NNP':
                  sent[0] = sent[0].lower()
                  sent2[0] = sent2[0].lower()
                sent = ' '.join(sent[:-1])
                sent = sent.replace('-LRB-','(').replace('-RRB-',')')
                if w[0]==replace[0]:
                  sent2 = re.sub(pattern,replace[1],' '.join(sent2[:-1]))
                  sent2 = re.sub(pattern2,' '+replace[1]+
                          'n',sent2) 
                else:
                  sent2 = re.sub(pattern,replace[0],' '.join(sent2[:-1]))
                  sent2 = re.sub(pattern2,' '+replace[0]+
                          'n',sent2)
                sent2 = sent2.replace('-LRB-','(').replace('-RRB-',')')
                qs.add(prefix+'did '+sent+'?')
                qs.add('Did '+sent2+'?')
                break
            break
          if found: break
        if found: break
  #print qs
  return qs

def getMoreYNQuestions(parsed,verbs):
  qs = set()
  i = 0
  auxVerbs = set(['was','is','are','were','will'])
  #ignore have, to avoid needing to figure out if it's auxiliary or transitive
  ignore = set(['has','had','have'])
  while i<len(parsed):
    tree = nltk.tree.ParentedTree.parse(parsed[i])
    if len(tree[0])==3 and tree[0][0].node=='NP' and tree[0][1].node=='VP':
      vp = tree[0][1]
      if vp[0].node[:2]=='VB' and vp[0][0] in ignore: 
        i+=1
        continue
      if vp[0].node=='MD' or (len(vp[0])==1 and vp[0].node[:2]=='VB' 
                             and vp[0][0] in auxVerbs):
        verb = vp[0][0]
        #remove verb phrase from tree
        tree[vp.treeposition()][0][0] = ''
        sent = [t for t in tree.leaves() if t.strip()]
        #make first word lowercase if not a proper noun
        if tree.pos()[0][1]!='NNP':
          sent[0] = sent[0].lower()
        sent = ' '.join(sent[:-1])
        sent = sent.replace('-LRB-','(').replace('-RRB-',')')
        qs.add(verb.capitalize()+' '+sent+'?')
      elif vp[0].node[:2]=='VB' and vp[0][0] in verbs:
        if vp[0].node[-1]=='D' or vp[0].node[-1]=='N':
          pref = 'Did '
        elif vp[0].node[-1]=='P':
          pref = 'Do '
        else: pref = 'Does '
        base = verbs[vp[0][0]]
        tree[vp.treeposition()][0][0] = base
        sent = [t for t in tree.leaves() if t.strip()]
        if tree.pos()[0][1]!='NNP':
          sent[0] = sent[0].lower()
        sent = ' '.join(sent[:-1])
        sent = sent.replace('-LRB-','(').replace('-RRB-',')')
        qs.add(pref+sent+'?')
    i+=1
  #print qs
  return qs

def getWhoQuestions(parsed, trees, names, n):
  qs = set()
  for tree in trees:
    if len(qs)>n: break
    if len(tree.leaves())<13: continue
    if (tree[0][0].node == 'NP'): #first right branch of tree is NP
      if tree[0][0][0].node == 'DT': continue #probably not a name
      for i in range(0,len(tree[0][0])):
        if len(tree[0][0][i])==2 and (tree[0][0][i][0].node=='NNP' and
           tree[0][0][i][1].node=='NNP'): #two names, check both are in list
           w1 = tree[0][0][i][0][0]
           w2 = tree[0][0][i][1][0]
           if (w1 in names and names[w1]<500) and (w2 in names 
               and names[w2]<1000):
             tree[0][0]=''
             qs.add('Who'+' '.join(tree.leaves())[:-2]+'?')
           break
        elif len(tree[0][0][i])==1 and tree[0][0][i].node =='NNP':
          w = tree[0][0][i][0].lower()
          if w in names and names[w]<1000: #check name rank
            tree[0][0] = ''
            qs.add('Who'+' '.join(tree.leaves())[:-2]+'?')
            break
        elif len(tree[0][0][i])==1 and tree[0][0][i].node=='PRP':
          pronoun = tree[0][0][i][0].lower()
          if pronoun=='he' or pronoun=='she':
            tree[0][0] = ''
            qs.add('Who'+' '.join(tree.leaves())[:-2]+'?')
            break
          #elif pronoun=='it':
          #  if sum(1 for i in tree.subtrees(lambda t: t.node=='SBAR'))>0: 
          #    continue
          #  tree[0][0]=''
          #  qs.add('What'+' '.join(tree.leaves())[:-2]+'?')
          #  break
  return qs

#Old version of yes/no, before parsing was added
def getYNQuestions(sentences,n):
  qs = set()
  pattern = re.compile('^(.*?)\W(was|is|are|were)(\W.*?)\W$', re.MULTILINE)
  for s in sentences:
    if len(qs)==n: break
    match = pattern.search(s)
    if match:
      tags = nltk.pos_tag(s)
      if tags[0][1][:3]=='NNP' or (tags[0][1]=='DT' and tags[1][1][:3]=='NNP'): 
        verb = ''
        for pair in tags:
          if pair[1][0]=='V': 
            verb = pair[0]
            break
        if verb!=match.group(2): continue
        q = match.group(2).title()+' '+match.group(1)+match.group(3)[:-1]+'?'
        qs.add(q)
  return qs

if __name__=='__main__':
  if(len(sys.argv) < 3):
    print "Usage: ./ask article.txt nquestions"
  else:
    main(sys.argv[1],int(sys.argv[2]))
