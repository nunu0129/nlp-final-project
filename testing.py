import PassageRetrieval, answerRetrieval, sys, tfidf

questionFile = open("testResults/teamQuestions.txt")
outFile = open(sys.argv[1] , 'w')

i = "0"
location = ""
questions = []
passages = []
probs = []
realAnswers = []

for line in questionFile:
    questionData = line.split()
    #first token is the number of the question; there are 2 of each
    if(i == questionData[0]):
        continue
    else:
        i = questionData[0]
    currLocation = questionData[1] + ".txt"
    if(currLocation != location and passages != []):
        answers = answerRetrieval.findAnswer(questions, passages, location)
        for i in range(len(answers)):
            outFile.write("question: " + questions[i] + "\n")
            outFile.write("real answer: " + realAnswers[i] + "\n")
            currPassages = passages[i]
            for (prob, passage) in currPassages:
                outFile.write(str(prob) + " " + passage + "\n")
            outFile.write("\nfound answer: " + answers[i] + "\n")
            outFile.write("\n\n\n")
        questions = []
        passages = []
        probs = []
        realAnswers = []
    if(currLocation != location):
        location = currLocation

    qMarkIndex = 4
    for j, word in enumerate(questionData):
        if("?" in word):
            qMarkIndex = j
    question = " ".join(questionData[3:qMarkIndex + 1])
    realAnswer = " ".join(questionData[qMarkIndex + 3 : -1])
    currPassages = tfidf.tfidf(question, 3, location)
#    currPassages = PassageRetrieval.stringMatch(question, 3, location)
    passages.append(currPassages)
    questions.append(question)
    realAnswers.append(realAnswer)

answers = answerRetrieval.findAnswer(questions, passages, location)
for i in range(len(answers)):
    outFile.write("question: " + questions[i] + "\n")
    outFile.write("real answer: " + realAnswers[i] + "\n")
    for (prob, passage) in currPassages:
        outFile.write(str(prob) + " " + passage + "\n")
    outFile.write("\nfound answer: " + answers[i] + "\n")
    outFile.write("\n\n")
