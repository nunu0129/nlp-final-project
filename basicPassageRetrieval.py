import nltk, re, sys

#takes a question (list of words) and a passage (list of sentences)
#finds the most relevant sentence in passage
def findPassage(question, passage):
    qSet = set(question)
    total = len(qSet)

    bestSentence = ""
    bestRate = 0.0
    for sentence in passage:
        pwords = set(nltk.word_tokenize(sentence))
        count = 0.0
        for word in pwords:
            if (word in qSet):
                count += 1
        rate = count/total
        if(rate > bestRate):
            bestRate = rate
            bestSentence = sentence

    return (bestSentence, bestRate)



if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: python passageRetrieval.py question(string) passage(file)"
    else:
        question = nltk.word_tokenize(sys.argv[1])
        passage = open(sys.argv[2])
        sentences = []
        for line in passage:
            newsents = nltk.sent_tokenize(line)
            sentences = sentences + newsents
        print findPassage(question,sentences)
