import PassageRetrieval, answerRetrieval, sys

questionFile = open("../questions.txt")
outFile = open(sys.argv[1] , 'w')

i = "0"
location = ""
questions = []
passages = []
probs = []
realAnswers = []

currLocation = "set1/a1.txt"

for line in questionFile:

    if(currLocation != location and passages != []):
        answers = answerRetrieval.findAnswer(questions, passages, probs)
        for i in range(len(answers)):
            outFile.write("question: " + questions[i] + "\n")
            outFile.write("real answer: " + realAnswers[i] + "\n")
            outFile.write("passage found: " + passages[i] + "\n")
            outFile.write("found answer: " + answers[i] + "\n")
            outFile.write("\n\n")
        questions = []
        passages = []
        probs = []
        realAnswers = []
    if(currLocation != location):
        location = currLocation

    question = line[:-1]
    realAnswer = "No."

    currPassages = PassageRetrieval.stringMatch(question, 1, location)
    for(prob, passage) in currPassages:
        probs.append(prob)
        passages.append(passage)
        questions.append(question)
        realAnswers.append(realAnswer)

answers = answerRetrieval.findAnswer(questions, passages, probs)
for i in range(len(answers)):
    outFile.write("question: " + questions[i] + "\n")
    outFile.write("real answer: " + realAnswers[i] + "\n")
    outFile.write("passage found: " + passages[i] + "\n")
    outFile.write("found answer: " + answers[i] + "\n")
    outFile.write("\n\n")
