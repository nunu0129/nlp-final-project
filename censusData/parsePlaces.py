import re

dataFile = open("places2k.txt")
citiesFile = open("cities.txt", 'w')

cityPat = re.compile('[A-Z][a-zA-Z]+')

cities = {}

for line in dataFile:
    for city in cityPat.finditer(line):
        name = city.group(0)
        if name in cities:
            cities[name] = cities[name] + 1
        else:
            cities[name] = 1

dataFile.close()

for city in cities:
    citiesFile.write(city + " " + str(cities[city]) + "\n")
